'use strict';
/**
 * made with bestemmie - blacktech 2020
 */
let status = 'pause'
const player = document.createElement("audio")
const el_button = document.getElementById('button')
const el_volume = document.getElementById('volume')

let source = document.createElement('source')
// TODO: provare l'opus perche' l'ogg su device vecchi ha comunque un lag di vari secondi
//if (player.canPlayType('audio/ogg;')) {
    //source.type= 'audio/ogg'
    // il Math.random e' un fix ad un bug assurdo che succede solo se:
    // - su firefox
    // - per stream ogg
    // - in https
    // - che sono stati gia' visti (questa e' l'unica che possiamo evitare)
    //source.src= 'https://s.streampunk.cc/blackout.ogg?' + Math.random()
//} else {
    source.type= 'audio/mpeg'
    source.src= 'https://s.streampunk.cc/blackout.mp3'
//}
player.appendChild(source)

// enough of the audio has loaded to allow playback to begin
player.addEventListener('canplaythrough', function () {
  el_button.classList.remove('loading')
})

el_button.addEventListener('click', function () {
  if (status === 'play') {
    player.load()
  } else {
    player.play()
  }
  status = status === 'play' ? 'pause' : 'play'
  el_button.classList.toggle('pause')
})

function changeVolume (v) {
  player.volume = volume.value/100
}

volume.addEventListener('mousemove', changeVolume)
volume.addEventListener('change', changeVolume)
